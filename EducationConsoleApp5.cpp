#include <iostream>
#include <cmath>

class Vector {
private:
    double x, y, z;
public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void ShowVectorValue() {
        std::cout << "\n" << "Vector:\n\nX: " << x << "\nY: " << y << "\nZ: " << z << "\n";
    }
    void ShowVectorLenght() {
        int VectorLenght = hypot(x, hypot(y, z));
        std::cout << "\nVector Lenght: " << VectorLenght << "\n";
    }
};

int main()
{
    Vector vect(2,4,6);
    vect.ShowVectorValue();
    vect.ShowVectorLenght();
}
